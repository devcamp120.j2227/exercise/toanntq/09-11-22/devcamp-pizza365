const getAllUserMiddleware = (request, response, next) => {
    console.log("Get ALL User Middleware");
    next();
}

const createUserMiddleware = (request, response, next) => {
    console.log("Create User Middleware");
    next();
}

const getDetailUserMiddleware = (request, response, next) => {
    console.log("Get Detail User Middleware");
    next();
}

const updateUserMiddleware = (request, response, next) => {
    console.log("Update User Middleware");
    next();
}

const deleteUserMiddleware = (request, response, next) => {
    console.log("Delete User Middleware");
    next();
}

module.exports = {
    getAllUserMiddleware,
    createUserMiddleware,
    getDetailUserMiddleware,
    updateUserMiddleware,
    deleteUserMiddleware
}