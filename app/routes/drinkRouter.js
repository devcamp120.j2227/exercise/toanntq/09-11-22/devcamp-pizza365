// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import drink middleware
const drinkMiddleware = require("../middlewares/drinkMiddleware");

router.get("/drinks", drinkMiddleware.getAllDrinkMiddleware, (request, response) => {
    response.json({
        message: "Get ALL drink"
    })
})

router.post("/drinks", drinkMiddleware.createDrinkMiddleware, (request, response) => {
    response.json({
        message: "Create drink"
    })
})

router.get("/drinks/:drinkId", drinkMiddleware.getDetailDrinkMiddleware, (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Get drink ID = " + drinkId
    })
})

router.put("/drinks/:drinkId", drinkMiddleware.updateDrinkMiddleware, (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Update drink ID = " + drinkId
    })
})

router.delete("/drinks/:drinkId", drinkMiddleware.deleteDrinkMiddleware, (request, response) => {
    const drinkId = request.params.drinkId;

    response.json({
        message: "Delete drink ID = " + drinkId
    })
})

module.exports = router;