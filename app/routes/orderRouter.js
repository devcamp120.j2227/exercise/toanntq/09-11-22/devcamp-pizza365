// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import order middleware
const orderMiddleware = require("../middlewares/orderMiddleware");

router.get("/orders", orderMiddleware.getAllOrderMiddleware, (request, response) => {
    response.json({
        message: "Get ALL orders"
    })
})

router.post("/orders", orderMiddleware.createOrderMiddleware, (request, response) => {
    response.json({
        message: "Create order"
    })
})

router.get("/orders/:orderId", orderMiddleware.getDetailOrderMiddleware, (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Get order ID = " + orderId
    })
})

router.put("/orders/:orderId", orderMiddleware.updateOrderMiddleware, (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Update order ID = " + orderId
    })
})

router.delete("/orders/:orderId", orderMiddleware.deleteOrderMiddleware, (request, response) => {
    const orderId = request.params.orderId;

    response.json({
        message: "Delete order ID = " + orderId
    })
})

module.exports = router;