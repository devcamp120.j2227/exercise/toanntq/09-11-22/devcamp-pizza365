// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import user middleware
const userMiddleware = require("../middlewares/userMiddleware");

router.get("/users", userMiddleware.getAllUserMiddleware, (request, response) => {
    response.json({
        message: "Get ALL Users"
    })
})

router.post("/users", userMiddleware.createUserMiddleware, (request, response) => {
    response.json({
        message: "Create user"
    })
})

router.get("/users/:userId", userMiddleware.getDetailUserMiddleware, (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Get user ID = " + userId
    })
})

router.put("/users/:userId", userMiddleware.updateUserMiddleware, (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Update user ID = " + userId
    })
})

router.delete("/users/:userId", userMiddleware.deleteUserMiddleware, (request, response) => {
    const userId = request.params.userId;

    response.json({
        message: "Delete user ID = " + userId
    })
})

module.exports = router;