// Khai báo thư viện ExpressJS
const express = require("express");

// Khai báo router app
const router = express.Router();

// Import voucher middleware
const voucherMiddleware = require("../middlewares/voucherMiddleware");

router.get("/vouchers", voucherMiddleware.getAllVoucherMiddleware, (request, response) => {
    response.json({
        message: "Get ALL voucher"
    })
})

router.post("/vouchers", voucherMiddleware.createVoucherMiddleware, (request, response) => {
    response.json({
        message: "Create voucher"
    })
})

router.get("/vouchers/:voucherId", voucherMiddleware.getDetailVoucherMiddleware, (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Get voucher ID = " + voucherId
    })
})

router.put("/vouchers/:voucherId", voucherMiddleware.updateVoucherMiddleware, (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Update voucher ID = " + voucherId
    })
})

router.delete("/vouchers/:voucherId", voucherMiddleware.deleteVoucherMiddleware, (request, response) => {
    const voucherId = request.params.voucherId;

    response.json({
        message: "Delete voucher ID = " + voucherId
    })
})

module.exports = router;